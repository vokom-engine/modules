package ee.dev.modules.sales.application.service.impl;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import ee.dev.modules.core.domain.model.Customer;
import ee.dev.modules.sales.application.service.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

@Slf4j
@Service
@PropertySource("classpath:mail.properties")
public class MailGunEmailService implements EmailService {

    @NotNull
    @Value("${mail.from}")
    private String from;

    @NotNull
    @Value("${mail.gun.api.key}")
    private String apiKey;

    @NotNull
    @Value("${greentickets.domain}")
    private String domain;

    @NotNull
    @Value("${mail.subject}")
    private String subject;

    @NotNull
    @Value("${mail.text}")
    private String text;

    @Override
    public void sendTicket(Customer customer, String filepath) {
        log.info("Sending ticket to: {}", customer);

        try {
            HttpResponse<JsonNode> request = Unirest.post("https://api.mailgun.net/v3/" + domain + "/messages")
                    .basicAuth("api", apiKey)
                    .queryString("from", from)
                    .queryString("to", customer.getEmail())
                    .queryString("subject", subject)
                    .queryString("text", text)
                    //.field("attachment", new File(filepath))
                    .asJson();

            JsonNode reponse = request.getBody();

            log.info("Sent email response: {}", reponse);
        }
        catch (UnirestException ex){
            log.error("Error Sending Ticket Email to '{}'", customer.getEmail());
        }
    }
}
