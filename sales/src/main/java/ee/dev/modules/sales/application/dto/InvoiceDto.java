/*
package ee.dev.modules.sales.application.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ee.dev.modules.core.application.mapper.Mapper;
import ee.dev.modules.payments.application.dto.PaymentMethodDto;
import ee.dev.modules.payments.domain.model.Transaction;
import ee.dev.modules.sales.domain.model.AmountDue;
import ee.dev.modules.sales.domain.model.Invoice;
import ee.dev.modules.sales.domain.model.InvoiceStatus;
import ee.dev.modules.sales.domain.model.PurchaseOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(force = true)
public class InvoiceDto {

    private String id;

    private String referenceNumber;

    @NonNull
    private LocalDateTime dueDateTime;

    @NonNull
    private InvoiceStatus status;

    private AmountDue amountDue;

    @NonNull
    private PurchaseOrder purchaseOrder;

    private List<PaymentMethodDto> paymentMethods;

    //@JsonIgnore
    private Transaction transaction;

    public static InvoiceDto toDto(Invoice invoice){
        return Mapper.getModelMapper().map(invoice, InvoiceDto.class);
    }

    public Invoice toEntity(){
        return Mapper.getModelMapper().map(this, Invoice.class);
    }
}
*/
