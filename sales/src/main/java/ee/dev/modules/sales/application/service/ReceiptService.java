package ee.dev.modules.sales.application.service;

import ee.dev.modules.core.application.service.GenericService;
import ee.dev.modules.sales.domain.model.Invoice;
import ee.dev.modules.sales.domain.model.Receipt;

import javax.servlet.http.HttpServletRequest;

public interface ReceiptService extends GenericService<Receipt> {
    Receipt paymentCompleted(Invoice invoice, HttpServletRequest req);
}
