package ee.dev.modules.sales.application.service;

import ee.dev.modules.core.application.service.GenericService;
import ee.dev.modules.sales.domain.model.Invoice;
import ee.dev.modules.sales.domain.model.PurchaseOrder;

public interface InvoiceService extends GenericService<Invoice> {

    Invoice getInvoiceByReferenceNumber(String reference);

    Invoice createInvoice(PurchaseOrder purchaseOrder);

}
