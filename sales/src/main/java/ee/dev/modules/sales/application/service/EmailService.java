package ee.dev.modules.sales.application.service;

import ee.dev.modules.core.domain.model.Customer;

public interface EmailService {
    void sendTicket(Customer customer, String filepath);
}
