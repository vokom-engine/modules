package ee.dev.modules.sales.domain.model;

import ee.dev.modules.core.domain.model.AuditingEntity;
import ee.dev.modules.core.domain.model.Money;
import ee.dev.modules.product.domain.model.Ticket;
import lombok.*;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
public class TicketOrder extends AuditingEntity {

    @NonNull
    @ManyToOne
    private Ticket ticket;

    @NonNull
    @Embedded
    private Money price;

    private int quantity;

    private String discountCode;
}
