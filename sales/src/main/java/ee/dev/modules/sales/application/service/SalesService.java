package ee.dev.modules.sales.application.service;

import ee.dev.modules.product.domain.model.Event;
import ee.dev.modules.sales.application.dto.PurchaseOrderDto;
import ee.dev.modules.sales.domain.model.Invoice;

import javax.servlet.http.HttpServletRequest;

public interface SalesService {
    Invoice startOrder(PurchaseOrderDto purchaseOrder, Event event, HttpServletRequest req);

    // Todo Temporary mock-up implementation
    String transactionComplete(String reference, HttpServletRequest req);
}
