package ee.dev.modules.sales.application.service.impl;

import ee.dev.modules.core.application.service.BaseGenericService;
import ee.dev.modules.core.domain.model.Customer;
import ee.dev.modules.core.domain.model.Language;
import ee.dev.modules.sales.application.service.EmailService;
import ee.dev.modules.sales.application.service.ReceiptService;
import ee.dev.modules.sales.domain.model.Invoice;
import ee.dev.modules.sales.domain.model.Receipt;
import ee.dev.modules.sales.domain.repository.ReceiptRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Service
public class ReceiptServiceImpl extends BaseGenericService<ReceiptRepository, Receipt> implements ReceiptService {

    private final EmailService emailService;

    @Autowired
    public ReceiptServiceImpl(ReceiptRepository repository, EmailService emailService) {
        super(repository);

        this.emailService = emailService;
    }

    @Override
    public Receipt paymentCompleted(Invoice invoice, HttpServletRequest req) {
        //Optional<String> optional = generateTicket(invoice);
        //String filename = optional.orElseThrow(() -> new PaymentException("Error Creating Travel Tickets"));

        //URL url = RequestUtil.getUrl(req, "/files/");
        //String ticketUrl = url + coreConfig.getTicketsFileFolder() + filename;

        Receipt receipt = createReceipt(invoice, "");

        emailTicket(receipt, "filename");
        //emailReceipt(receipt.getTicketUrl());

        return receipt;
    }

    private void emailTicket(Receipt receipt, String fileName){
        Customer customer = receipt.getInvoice().getPurchaseOrder().getCustomer();
        //String path = getTicketPath(fileName);
        emailService.sendTicket(customer, "path");
    }

    /*private String getTicketPath(String fileName){
        String ticketsDirPath = coreConfig.getTempFolderPath() + coreConfig.getTicketsFileFolder();
        return ticketsDirPath + (ticketsDirPath.endsWith("/") ? "" : "/") + fileName;
    }*/

    private Receipt createReceipt(Invoice invoice, String ticketUrl) throws IllegalStateException {
        try {
            Receipt receipt = new Receipt();
            receipt.setInvoice(invoice);
            receipt.setLanguage(Language.EN);
            receipt.setTicketUrl(ticketUrl);

            return super.create(receipt);
        }
        catch (Exception ex){
            String errorMsg = "Error Creating Receipt";

            log.error(errorMsg, ex);

            throw new IllegalStateException(errorMsg);
        }
    }
}
