package ee.dev.modules.sales.application.service.impl;

import ee.dev.modules.core.utils.UrlUtil;
import ee.dev.modules.payments.application.dto.PaymentMethodDto;
import ee.dev.modules.payments.application.dto.PaymentRequestDto;
import ee.dev.modules.payments.application.service.PaymentService;
import ee.dev.modules.payments.domain.model.PaymentStatus;
import ee.dev.modules.payments.domain.model.Transaction;
import ee.dev.modules.product.domain.model.Event;
import ee.dev.modules.sales.application.dto.PurchaseOrderDto;
import ee.dev.modules.sales.application.service.InvoiceService;
import ee.dev.modules.sales.application.service.PurchaseOrderService;
import ee.dev.modules.sales.application.service.ReceiptService;
import ee.dev.modules.sales.application.service.SalesService;
import ee.dev.modules.sales.domain.model.Invoice;
import ee.dev.modules.sales.domain.model.PurchaseOrder;
import ee.dev.modules.sales.domain.model.Receipt;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.URL;
import java.util.List;

@Slf4j
@Service
public class SalesServiceImpl implements SalesService {
    private final PurchaseOrderService purchaseOrderService;
    private final InvoiceService invoiceService;
    private final PaymentService paymentService;
    private final ReceiptService receiptService;

    @Autowired
    public SalesServiceImpl(PurchaseOrderService purchaseOrderService, InvoiceService invoiceService,
                            PaymentService paymentService, ReceiptService receiptService) {
        this.purchaseOrderService = purchaseOrderService;
        this.invoiceService = invoiceService;
        this.paymentService = paymentService;
        this.receiptService = receiptService;
    }

    @Override
    public Invoice startOrder(PurchaseOrderDto purchaseOrder, Event event, HttpServletRequest req) {
        PurchaseOrder order = purchaseOrderService.createPurchaseOrder(purchaseOrder, event);
        Invoice invoice = invoiceService.createInvoice(order);

        URL baseUrl = getBaseUrl(req);

        String returnUrl = buildPaymentReturnUrl(baseUrl, invoice.getReferenceNumber());
        Validate.notNull(returnUrl, "Payment return url cannot be null");

        PaymentRequestDto request = buildPaymentRequest(invoice, returnUrl);

        Transaction transaction = paymentService.startPaymentTransaction(request);
        invoice.setTransaction(transaction);

        Invoice updatedInvoice = invoiceService.update(invoice);

        //InvoiceDto invoiceDto = InvoiceDto.toDto(invoiceService.update(invoice));

        List<PaymentMethodDto> paymentMethods = paymentService.getPaymentMethods(transaction, returnUrl, baseUrl);
        updatedInvoice.setPaymentMethods(paymentMethods);

        return updatedInvoice;
    }

    @Override
    public String transactionComplete(String reference, HttpServletRequest req) {
        Validate.notNull(reference, "Reference cannot be null");

        // Todo Mock payment validation
        Invoice invoice = invoiceService.getInvoiceByReferenceNumber(reference);
        Validate.notNull(invoice, "Invalid invoice reference");

        invoice.getTransaction().setPaymentStatus(PaymentStatus.COMPLETED);
        invoice.getTransaction().setAmountPaid(invoice.getTransaction().getAmountDue());

        Invoice updatedInvoice = invoiceService.update(invoice);

        Receipt receipt = receiptService.paymentCompleted(updatedInvoice, req);

        return buildClientReturnUrl(updatedInvoice.getPurchaseOrder(), receipt.getId());
    }

    private PaymentRequestDto buildPaymentRequest(Invoice invoice, String returnUrl) {

        PaymentRequestDto request = new PaymentRequestDto();
        request.setAmount(invoice.getAmountDue().getGrandTotal());
        request.setCustomerName(invoice.getPurchaseOrder().getCustomer().getFullName());
        request.setReference(invoice.getReferenceNumber());
        request.setReturnUrl(returnUrl);
        request.setCountry(invoice.getPurchaseOrder().getCountry());

        return request;
    }

    private String buildClientReturnUrl(PurchaseOrder purchaseOrder, String receiptId) {
        return String.format("%s?rid=%s&lg=%s", purchaseOrder.getReturnUrl(), receiptId, purchaseOrder.getLanguage().name());
    }

    // Todo mock return url
    private String buildPaymentReturnUrl(URL baseUrl, String reference) {
        try {
            URL returnUrl = UrlUtil.combine(baseUrl, "/api/payments/payment-complete?reference=" + reference);

            return returnUrl.toURI().toString();
        }
        catch (Exception ex){
            log.error("Exception building payment return url: {}", ex.getMessage(), ex);
        }

        return null;
    }

    private URL getBaseUrl(HttpServletRequest req) {
        try {
            return UrlUtil.getBaseUrl(req);
        }
        catch (Exception ex){
            log.error("Exception building base url: {}", ex.getMessage(), ex);
        }

        return null;
    }
}
