package ee.dev.modules.sales.application.dto;

import ee.dev.modules.core.domain.model.Money;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(force = true)
public class TicketOrderDto {

    @NonNull
    private String ticketId;

    private String discountCode;

    @NonNull
    private Money price;

    private int quantity;
}
