package ee.dev.modules.sales.application.service.impl;

import ee.dev.modules.core.application.service.BaseGenericService;
import ee.dev.modules.core.domain.model.Money;
import ee.dev.modules.sales.application.service.InvoiceService;
import ee.dev.modules.sales.domain.model.AmountDue;
import ee.dev.modules.sales.domain.model.Invoice;
import ee.dev.modules.sales.domain.model.InvoiceStatus;
import ee.dev.modules.sales.domain.model.PurchaseOrder;
import ee.dev.modules.sales.domain.repository.InvoiceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Slf4j
@Service
public class InvoiceServiceImpl extends BaseGenericService<InvoiceRepository, Invoice> implements InvoiceService {
    private static final long PAYMENT_DUE_TIME_MINUTES = 15;

    @Autowired
    public InvoiceServiceImpl(InvoiceRepository repository) {
        super(repository);
    }

    @Override
    public Invoice getInvoiceByReferenceNumber(String reference) {
        return repository.getInvoiceByReferenceNumber(reference);
    }

    @Override
    public Invoice createInvoice(PurchaseOrder purchaseOrder) {

        AmountDue amountDue = calculateAmountDue(purchaseOrder.getTotalPrice());

        LocalDateTime dueDateTime = ZonedDateTime.now().toLocalDateTime();

        Invoice invoice = new Invoice();
        invoice.setReferenceNumber(purchaseOrder.getOrderNumber());
        invoice.setDueDateTime(dueDateTime.plusMinutes(PAYMENT_DUE_TIME_MINUTES));
        invoice.setStatus(InvoiceStatus.ACTIVE);
        invoice.setAmountDue(amountDue);
        invoice.setPurchaseOrder(purchaseOrder);

        return super.create(invoice);
    }

    private AmountDue calculateAmountDue(Money total) {
        Money vat = calculateVat(total);
        Money grandTotal = Money.of(total.getAmount().add(vat.getAmount()), total.getCurrency());

        AmountDue amountDue = new AmountDue();
        amountDue.setTotal(total);
        amountDue.setTax(vat);
        amountDue.setGrandTotal(grandTotal);

        return amountDue;
    }

    private Money calculateVat(Money total){
        return Money.of(total.getAmount().multiply(getVat()), total.getCurrency());
    }

    private BigDecimal getVat() {
        return BigDecimal.valueOf(0.2);
    }

}
