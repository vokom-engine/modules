package ee.dev.modules.sales.application.dto;

import ee.dev.modules.core.domain.model.Country;
import ee.dev.modules.core.domain.model.Customer;
import ee.dev.modules.core.domain.model.Language;
import ee.dev.modules.core.domain.model.Money;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.List;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(force = true)
public class PurchaseOrderDto {

    private int totalQuantity;

    @NonNull
    private String returnUrl;

    @NonNull
    private String eventId;

    @NonNull
    private Customer customer;

    @NonNull
    private Country country;

    @NonNull
    private Language language;

    @NonNull
    private Money totalPrice;

    @NonNull
    private List<TicketOrderDto> ticketOrders;
}
