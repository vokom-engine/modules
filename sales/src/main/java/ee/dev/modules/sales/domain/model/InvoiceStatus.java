package ee.dev.modules.sales.domain.model;

/**
 * Created by bilal90.
 */
public enum InvoiceStatus {
    ACTIVE,
    INACTIVE,
    SETTLED,
    EXPIRED
}
