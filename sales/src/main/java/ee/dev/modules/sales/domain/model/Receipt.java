package ee.dev.modules.sales.domain.model;

import ee.dev.modules.core.domain.model.AuditingEntity;
import ee.dev.modules.core.domain.model.Language;
import lombok.*;

import javax.persistence.*;

/**
 * Created by bilal90.
 */
@Entity
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
public class Receipt extends AuditingEntity {

    @Column
    private String ticketUrl;

    @NonNull
    @ManyToOne
    private Invoice invoice;

    @Column
    @Enumerated(EnumType.STRING)
    private Language language;
}
