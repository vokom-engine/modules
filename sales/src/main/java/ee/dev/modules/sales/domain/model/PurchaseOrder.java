package ee.dev.modules.sales.domain.model;

import ee.dev.modules.core.domain.model.*;
import ee.dev.modules.product.domain.model.Event;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
public class PurchaseOrder extends AuditingEntity {

    private int totalQuantity;

    @NotEmpty
    @NonNull
    private String orderNumber;

    @NotEmpty
    @NonNull
    private String returnUrl;

    @NonNull
    private LocalDateTime orderDateTime;

    @NonNull
    @Enumerated(EnumType.STRING)
    private Country country;

    @NonNull
    @Enumerated(EnumType.STRING)
    private Language language;

    @Embedded
    private Money totalPrice;

    @NonNull
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Customer customer;

    @NonNull
    @ManyToOne
    private Event event;

    @NonNull
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<TicketOrder> ticketOrders;
}
