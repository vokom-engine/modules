package ee.dev.modules.sales.domain.repository;

import ee.dev.modules.sales.domain.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, String> {
    Invoice getInvoiceByReferenceNumber(String reference);
}
