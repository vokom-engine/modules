package ee.dev.modules.sales.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import ee.dev.modules.core.domain.model.BaseEntity;
import ee.dev.modules.core.domain.model.Money;
import lombok.*;

import javax.persistence.*;

@Embeddable
@Data
@Table
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
public class AmountDue extends BaseEntity {

    @NonNull
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="amount", column=@Column(name="total_amount")),
            @AttributeOverride(name="currency", column=@Column(name="total_currency"))
    })
    private Money total;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="amount", column=@Column(name="other_amount")),
            @AttributeOverride(name="currency", column=@Column(name="other_currency"))
    })
    private Money otherCharges;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="amount", column=@Column(name="tax_amount")),
            @AttributeOverride(name="currency", column=@Column(name="tax_currency"))
    })
    private Money tax;

    @NonNull
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="amount", column=@Column(name="grand_total_amount")),
            @AttributeOverride(name="currency", column=@Column(name="grand_total_currency"))
    })
    private Money grandTotal;
}
