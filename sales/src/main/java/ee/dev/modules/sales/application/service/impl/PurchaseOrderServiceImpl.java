package ee.dev.modules.sales.application.service.impl;

import ee.dev.modules.core.application.service.BaseGenericService;
import ee.dev.modules.core.domain.model.Customer;
import ee.dev.modules.core.domain.model.Money;
import ee.dev.modules.core.utils.RandomString;
import ee.dev.modules.product.application.service.TicketService;
import ee.dev.modules.product.domain.model.Event;
import ee.dev.modules.product.domain.model.EventStatus;
import ee.dev.modules.product.domain.model.Ticket;
import ee.dev.modules.product.domain.model.TicketStatus;
import ee.dev.modules.sales.application.dto.PurchaseOrderDto;
import ee.dev.modules.sales.application.dto.TicketOrderDto;
import ee.dev.modules.sales.application.service.PurchaseOrderService;
import ee.dev.modules.sales.domain.model.PurchaseOrder;
import ee.dev.modules.sales.domain.model.TicketOrder;
import ee.dev.modules.sales.domain.repository.PurchaseOrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PurchaseOrderServiceImpl extends BaseGenericService<PurchaseOrderRepository, PurchaseOrder> implements PurchaseOrderService {
    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;
    private static final int DECIMAL_PLACES = 2;

    //private final EventService<? extends Event> eventService;
    private final TicketService ticketService;

    @Autowired
    public PurchaseOrderServiceImpl(PurchaseOrderRepository repository, /*EventService<? extends Event> eventService,*/
                                    TicketService ticketService) {
        super(repository);

        //this.eventService = eventService;
        this.ticketService = ticketService;
    }

    @Override
    public PurchaseOrder createPurchaseOrder(PurchaseOrderDto purchaseOrder, Event event) {
        Validate.notNull(purchaseOrder, "PurchaseOrder cannot be null");
        Validate.isTrue(StringUtils.isNotBlank(purchaseOrder.getReturnUrl()), "ReturnUrl cannot be empty");

        log.info("Create purchase order for: {}", purchaseOrder);

        validateCustomer(purchaseOrder.getCustomer());
        validateEvent(event);

        List<TicketOrder> ticketOrders = createTicketOrders(purchaseOrder.getTicketOrders());

        Money totalPrice = calculateTotalPrice(ticketOrders, purchaseOrder.getTotalPrice());
        int totalQuantity = getTotalQuantity(ticketOrders, purchaseOrder.getTotalQuantity());

        PurchaseOrder order = new PurchaseOrder();
        order.setOrderNumber(generateOrderNumber());
        order.setOrderDateTime(ZonedDateTime.now().toLocalDateTime());
        order.setCustomer(purchaseOrder.getCustomer());
        order.setEvent(event);
        order.setTotalQuantity(totalQuantity);
        order.setTotalPrice(totalPrice);
        order.setTicketOrders(ticketOrders);
        order.setCountry(purchaseOrder.getCountry());
        order.setLanguage(purchaseOrder.getLanguage());
        order.setReturnUrl(purchaseOrder.getReturnUrl());

        return super.create(order);
    }

    private List<TicketOrder> createTicketOrders(List<TicketOrderDto> ticketOrders){
        Validate.notEmpty(ticketOrders, "No Tickets found in submitted purchase order");

        return ticketOrders.stream().map(this::createTicketOrder).collect(Collectors.toList());
    }

    private TicketOrder createTicketOrder(TicketOrderDto ticketOrder){
        log.info("Create ticket order for: {}", ticketOrder);

        Ticket ticket = ticketService.findOne(ticketOrder.getTicketId());
        Validate.notNull(ticket, "No ticket found with id: " + ticketOrder.getTicketId());

        Validate.isTrue(ticket.getStatus().equals(TicketStatus.AVAILABLE), "Unavailable ticket with id: " + ticket.getId());

        int quantity = ticketOrder.getQuantity();
        Validate.isTrue(quantity > 0, "Order quantity must be greater than 0");

        Validate.isTrue(quantity <= ticket.getAvailable(), "Order quantity is greater than available tickets");

        Money price = calculateTicketOrderPrice(ticket.getPrice(), ticketOrder.getPrice(), new BigDecimal(quantity));

        log.info("Calculated ticket order price: {}", price);

        // Todo Factor discount code in calculation

        return TicketOrder.of(ticket, price, quantity, ticketOrder.getDiscountCode());
    }

    private Money calculateTotalPrice(List<TicketOrder> ticketOrders, Money totalPrice){
        BigDecimal calculatedTotal = ticketOrders.stream().map(x -> x.getPrice().getAmount())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        validateEqualAmount(calculatedTotal, totalPrice.getAmount());

        return Money.of(scaleAmount(calculatedTotal), ticketOrders.get(0).getPrice().getCurrency());
    }

    private Money calculateTicketOrderPrice(Money ticketPrice, Money orderPrice, BigDecimal quantity){

        BigDecimal calculatedPrice = ticketPrice.getAmount().multiply(quantity);

        validateEqualAmount(calculatedPrice, orderPrice.getAmount());

        return Money.of(scaleAmount(calculatedPrice), ticketPrice.getCurrency());
    }

    private int getTotalQuantity(List<TicketOrder> ticketOrders, int totalQuantity){
        int calculatedTotalQuantity = ticketOrders.stream().map(TicketOrder::getQuantity).reduce(0, Integer::sum);

        Validate.isTrue(calculatedTotalQuantity == totalQuantity, "Incorrect total quatity");

        return calculatedTotalQuantity;
    }

    private void validateCustomer(Customer customer){
        Validate.notEmpty(customer.getFirstName(), "Customer name cannot be empty");
        Validate.isTrue(EmailValidator.getInstance().isValid(customer.getEmail()), "Customer email is not valid");
    }

    private void validateEvent(Event event){
        log.info("Validate event with id: {}", event.getId());

        Validate.notNull(event, "Event cannot be null");
        Validate.isTrue(event.getStatus().equals(EventStatus.ACTIVE), "Event is not active: " + event.getId());
    }

    private BigDecimal scaleAmount(BigDecimal amount) {
        return amount.setScale(DECIMAL_PLACES, ROUNDING_MODE);
    }

    private void validateEqualAmount(BigDecimal amount1, BigDecimal amount2) {
        boolean isEqual = scaleAmount(amount1).compareTo(scaleAmount(amount2)) == 0;

        Validate.isTrue(isEqual, String.format("Unequal values of %s and %s", amount1, amount2));
    }

    private String generateOrderNumber(){
        RandomString randomString = RandomString.randomUpperString(4);
        return randomString.nextString() + RandomString.random(4) + "PPL";
    }
}
