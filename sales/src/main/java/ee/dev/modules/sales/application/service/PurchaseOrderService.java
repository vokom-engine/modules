package ee.dev.modules.sales.application.service;

import ee.dev.modules.core.application.service.GenericService;
import ee.dev.modules.product.domain.model.Event;
import ee.dev.modules.sales.application.dto.PurchaseOrderDto;
import ee.dev.modules.sales.domain.model.PurchaseOrder;

public interface PurchaseOrderService extends GenericService<PurchaseOrder> {
    PurchaseOrder createPurchaseOrder(PurchaseOrderDto purchaseOrder, Event event);
}
