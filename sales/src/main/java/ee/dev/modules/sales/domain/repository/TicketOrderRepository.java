package ee.dev.modules.sales.domain.repository;

import ee.dev.modules.sales.domain.model.TicketOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketOrderRepository extends JpaRepository<TicketOrder, String> {

}
