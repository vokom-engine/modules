package ee.dev.modules.sales.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonInclude;
import ee.dev.modules.core.domain.model.AuditingEntity;
import ee.dev.modules.payments.application.dto.PaymentMethodDto;
import ee.dev.modules.payments.domain.model.Transaction;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
public class Invoice extends AuditingEntity {

    @NonNull
    private String referenceNumber;

    @NonNull
    private LocalDateTime dueDateTime;

    @NonNull
    @Enumerated(EnumType.STRING)
    private InvoiceStatus status;

    @Embedded
    private AmountDue amountDue;

    @OneToOne
    private Transaction transaction;

    @NonNull
    @ManyToOne
    private PurchaseOrder purchaseOrder;

    @Transient
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<PaymentMethodDto> paymentMethods;
}
