package ee.dev.modules.product.application.service.impl;

import ee.dev.modules.core.application.service.BaseGenericService;
import ee.dev.modules.product.application.service.TicketService;
import ee.dev.modules.product.domain.model.Ticket;
import ee.dev.modules.product.domain.repository.TicketRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 */

@Slf4j
@Service
public class TicketServiceImpl extends BaseGenericService<TicketRepository, Ticket> implements TicketService {

    @Autowired
    public TicketServiceImpl(TicketRepository repository) {
        super(repository);
    }
}
