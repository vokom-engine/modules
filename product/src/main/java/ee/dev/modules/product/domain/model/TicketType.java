package ee.dev.modules.product.domain.model;

/**
 *
 */
public enum TicketType {
    FULL,
    DISCOUNTED,
    ZERO
}
