package ee.dev.modules.product.domain.model;

import ee.dev.modules.core.domain.model.AuditingEntity;
import ee.dev.modules.core.domain.model.Money;
import lombok.*;

import javax.persistence.*;

/**
 *
 */
@Entity
@Data
@Table(name = "tickets")
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
public class Ticket extends AuditingEntity {

    @Column
    private int total;

    @Column
    private int available;

    @NonNull
    @Embedded
    private Money price;

    @NonNull
    @Enumerated(EnumType.STRING)
    private TicketType type;

    @NonNull
    @Enumerated(EnumType.STRING)
    private TicketCategory category;

    @NonNull
    @Enumerated(EnumType.STRING)
    private TicketStatus status;
}
