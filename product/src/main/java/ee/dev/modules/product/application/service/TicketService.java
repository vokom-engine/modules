package ee.dev.modules.product.application.service;

import ee.dev.modules.core.application.service.GenericService;
import ee.dev.modules.product.domain.model.Ticket;

/**
 *
 */
public interface TicketService extends GenericService<Ticket> {

}
