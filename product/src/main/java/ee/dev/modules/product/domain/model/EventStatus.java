package ee.dev.modules.product.domain.model;

/**
 *
 */
public enum EventStatus {
    ACTIVE,
    INACTIVE,
    PAST,
    CANCELED,
    SUSPENDED
}
