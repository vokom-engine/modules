package ee.dev.modules.product.domain.model;

import ee.dev.modules.core.domain.model.AuditingEntity;
import ee.dev.modules.core.domain.model.DateTimePeriod;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public abstract class Event extends AuditingEntity {

    @NotEmpty
    @NonNull
    @Column
    protected String name;

    @NonNull
    @Embedded
    protected DateTimePeriod dateTimePeriod;

    @NonNull
    @Enumerated(EnumType.STRING)
    protected EventStatus status;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    protected Set<Ticket> tickets = new HashSet<>();
}
