package ee.dev.modules.product.domain.model;

/**
 *
 */
public enum TicketStatus {
    AVAILABLE,
    INACTIVE,
    SOLD_OUT
}
