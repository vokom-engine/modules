package ee.dev.modules.product.application.service;

import ee.dev.modules.core.application.service.GenericService;
import ee.dev.modules.product.domain.model.Event;

/**
 *
 */
public interface EventService<T extends Event> extends GenericService<T> {

}
