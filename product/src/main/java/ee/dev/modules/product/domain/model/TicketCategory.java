package ee.dev.modules.product.domain.model;

/**
 *
 */
public enum TicketCategory {
    STANDARD,
    VIP
}
