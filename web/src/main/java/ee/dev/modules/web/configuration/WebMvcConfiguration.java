package ee.dev.modules.web.configuration;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Created by bilal90 on 9/30/2017.
 */
@Configuration
public class WebMvcConfiguration {
    @Bean
    public FilterRegistrationBean corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");

        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(0);

        return bean;
    }
   /* @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {

            *//*@Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*");
            }*//*

            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                registry
                        .addResourceHandler("/resources/**")
                        .addResourceLocations("/resources/");

               *//* registry.addResourceHandler("/files/**")
                        .addResourceLocations("file:" + tempFolderPath);*//*

               *//* registry.addResourceHandler("/api/**")
                        .addResourceLocations("file:" + tempFolderPath + CoreAppGlobal.EXT_FOLDER);*//*


//                registry.addResourceHandler("/files2/**")
//                        .addResourceLocations("file:/Users/bilal/bilal/greentickets/code/new-engine/temp/image-files/",
//                                "file:/Users/bilal/bilal/greentickets/code/new-engine/temp/");

                super.addResourceHandlers(registry);
            }
        };
    }*/
}
