package ee.dev.modules.web.utils;

import ee.dev.modules.core.utils.FileAttribute;
import ee.dev.modules.core.utils.FileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public final class ResponseUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResponseUtil.class);

    private ResponseUtil() {
    }

    public static ResponseEntity<Map<String,String>> exceptionResponseBuilder(HttpStatus status, Exception ex){
        Map<String,String> response = new HashMap<>();

        response.put("timestamp", getTimeStamp());
        response.put("status", String.valueOf(status.value()));
        response.put("error", status.getReasonPhrase());
        response.put("exception", ex.getClass().getName());
        response.put("message", ex.getMessage());

        return new ResponseEntity<>(response, status);
    }

    public static void addFileResponse(File file, HttpServletResponse response) throws IOException {
        if(!file.exists()){
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);

            return;
        }

        String fileName = file.getName();
        response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        //response.setHeader("Content-disposition", "attachment; filename="+ fullname);
        response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");
        response.setContentLengthLong(file.length());

        String mimeType = FileUtil.getContentType(file);
        FileAttribute fileAttribute = new FileAttribute(file);
        String date = fileAttribute.getCreationTime();
        String lastModifiedDate = fileAttribute.getLastModifiedTime();

        response.setContentType(mimeType);
        response.setHeader("Date", date);
        response.setHeader("Last-modified", lastModifiedDate);

        Files.copy(file.toPath(), response.getOutputStream());
    }

    public static String getTimeStamp(){
        long time = new Timestamp(System.currentTimeMillis()).getTime();
        return String.valueOf(time);
    }

    public static <X> ResponseEntity<X> wrapOrNotFound(Optional<X> maybeResponse) {
        return wrapOrNotFound(maybeResponse, (HttpHeaders)null);
    }

    public static <X> ResponseEntity<X> wrapOrNotFound(Optional<X> maybeResponse, HttpHeaders header) {
        return maybeResponse.map(x -> (ResponseEntity.ok().headers(header)).body(x))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
