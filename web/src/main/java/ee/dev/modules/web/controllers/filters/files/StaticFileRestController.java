package ee.dev.modules.web.controllers.filters.files;

import ee.dev.modules.web.application.service.FileService;
import ee.dev.modules.web.controllers.filters.RestControllerExceptionFilter;
import ee.dev.modules.web.utils.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created by bilal90 on 9/6/2019.
 */

@CrossOrigin
@RestController
@RequestMapping("/files")
public class StaticFileRestController extends RestControllerExceptionFilter {
    private final FileService fileService;

    @Autowired
    public StaticFileRestController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/{path}/{name}")
    @ResponseStatus(HttpStatus.OK)
    public void getFile(HttpServletResponse response, @PathVariable(name = "path") String path,
            @PathVariable(name = "name") String name) throws IOException {

        File file = fileService.getFile(path, name);
        ResponseUtil.addFileResponse(file, response);
    }
}