package ee.dev.modules.web.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {
        WebPackageConfig.CORE + ".domain.repository",
})
@EntityScan(basePackages = {
        WebPackageConfig.CORE +  ".domain",
})
@ComponentScan(basePackages = {
        WebPackageConfig.CORE + ".*",
})
@PropertySource("classpath:application.properties")
public class WebPackageConfig {

    static final String CORE = "ee.peeply.dev.core";

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfig() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
