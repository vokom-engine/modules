package ee.dev.modules.web.application.service.impl;

import ee.dev.modules.core.configuration.FolderConfiguration;
import ee.dev.modules.web.application.service.FileService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class FileServiceImpl implements FileService {
    private final FolderConfiguration folderConfiguration;

    @Autowired
    public FileServiceImpl(FolderConfiguration folderConfiguration) {
        this.folderConfiguration = folderConfiguration;
    }

    @Override
    public File getFile(String path, String name) {
        String directory = String.format("%s%s", folderConfiguration.getFilesFolder(), path);
        File file = FileUtils.getFile(directory, name);
        if(!file.exists()){
            throw new IllegalArgumentException("Invalid file name");
        }

        return file;
    }

}
