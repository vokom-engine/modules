package ee.dev.modules.web.application.service;

import java.io.File;

public interface FileService {
    File getFile(String path, String name);
}
