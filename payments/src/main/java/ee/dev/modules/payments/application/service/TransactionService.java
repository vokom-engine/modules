package ee.dev.modules.payments.application.service;


import ee.dev.modules.core.application.service.GenericService;
import ee.dev.modules.payments.domain.model.Transaction;

/**
 *
 */
public interface TransactionService extends GenericService<Transaction> {

}
