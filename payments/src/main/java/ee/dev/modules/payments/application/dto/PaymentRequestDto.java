package ee.dev.modules.payments.application.dto;

import ee.dev.modules.core.domain.model.Country;
import ee.dev.modules.core.domain.model.Money;
import lombok.*;

/**
 * Created by bilal90.
 */
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(force = true)
@EqualsAndHashCode(callSuper=false)
public class PaymentRequestDto {

    @NonNull
    private String reference;

    @NonNull
    private String customerName;

    @NonNull
    private String returnUrl;

    private String additionalData;

    @NonNull
    private Country country;

    @NonNull
    private Money amount;
}
