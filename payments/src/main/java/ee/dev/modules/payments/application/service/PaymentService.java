package ee.dev.modules.payments.application.service;

import ee.dev.modules.payments.application.dto.PaymentMethodDto;
import ee.dev.modules.payments.application.dto.PaymentRequestDto;
import ee.dev.modules.payments.domain.model.Transaction;

import java.net.URL;
import java.util.List;

public interface PaymentService {

    Transaction startPaymentTransaction(PaymentRequestDto request);

    Transaction paymentTransactionComplete(String json);

    List<PaymentMethodDto> getPaymentMethods(Transaction transaction, String returnUrl, URL baseUrl);
}
