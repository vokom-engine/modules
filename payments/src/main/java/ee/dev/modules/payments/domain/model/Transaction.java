package ee.dev.modules.payments.domain.model;

import ee.dev.modules.core.domain.model.AuditingEntity;
import ee.dev.modules.core.domain.model.Country;
import ee.dev.modules.core.domain.model.Money;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
public class Transaction extends AuditingEntity {

    @NotEmpty
    @NonNull
    private String reference;

    @NotEmpty
    @NonNull
    private String customerName;

    @NonNull
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="amount", column=@Column(name="due_amount")),
            @AttributeOverride(name="currency", column=@Column(name="due_currency"))
    })
    private Money amountDue;

    @NonNull
    private LocalDateTime dateTime;

    @NonNull
    private PaymentStatus paymentStatus;

    @NonNull
    private Country country;

    private String transactionId;

    private String additionalData;

    private String shop;

    private String signature;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="amount", column=@Column(name="paid_amount")),
            @AttributeOverride(name="currency", column=@Column(name="paid_currency"))
    })
    private Money amountPaid;

}
