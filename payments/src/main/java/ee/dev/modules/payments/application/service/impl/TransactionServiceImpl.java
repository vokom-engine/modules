package ee.dev.modules.payments.application.service.impl;

import ee.dev.modules.core.application.service.BaseGenericService;
import ee.dev.modules.payments.application.service.TransactionService;
import ee.dev.modules.payments.domain.model.Transaction;
import ee.dev.modules.payments.domain.repository.TransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 */

@Slf4j
@Service
public class TransactionServiceImpl extends BaseGenericService<TransactionRepository, Transaction> implements TransactionService {

    @Autowired
    public TransactionServiceImpl(TransactionRepository repository) {
        super(repository);
    }
}
