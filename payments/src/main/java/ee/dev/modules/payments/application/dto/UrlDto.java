package ee.dev.modules.payments.application.dto;

import lombok.*;
import org.springframework.http.HttpMethod;

/**
 * Created by bilal90
 */
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper=false)
public class UrlDto {

    @NonNull
    private String url;

    @NonNull
    private HttpMethod method;
}
