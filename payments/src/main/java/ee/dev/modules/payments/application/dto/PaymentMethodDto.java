package ee.dev.modules.payments.application.dto;

import ee.dev.modules.core.domain.model.Country;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(force = true)
public class PaymentMethodDto {

    private String name;

    private String url;

    private String logo;

    private Country country;

    private PaymentTypeDto paymentType;
}
