package ee.dev.modules.payments.application.dto;

public enum PaymentTypeDto {
    BANK,
    CASH,
    CARD,
    MOBILE,
    OTHER;
}
