package ee.dev.modules.payments.domain.model;

/**
 * Created by bilal90.
 */
public enum PaymentStatus {
    STARTED,
    PENDING,
    CANCELLED,
    EXPIRED,
    COMPLETED,
    PAID_PARTLY,
    PAID_FULLY,
    PART_REFUNDED,
    REFUNDED,
}
