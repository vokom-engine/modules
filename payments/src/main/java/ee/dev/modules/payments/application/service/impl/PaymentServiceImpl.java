package ee.dev.modules.payments.application.service.impl;

import ee.dev.modules.core.domain.model.Country;
import ee.dev.modules.core.utils.UrlUtil;
import ee.dev.modules.payments.application.dto.PaymentMethodDto;
import ee.dev.modules.payments.application.dto.PaymentRequestDto;
import ee.dev.modules.payments.application.dto.PaymentTypeDto;
import ee.dev.modules.payments.application.service.PaymentService;
import ee.dev.modules.payments.application.service.TransactionService;
import ee.dev.modules.payments.domain.model.PaymentStatus;
import ee.dev.modules.payments.domain.model.Transaction;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class PaymentServiceImpl implements PaymentService {

    private final TransactionService transactionService;

    public PaymentServiceImpl(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public Transaction startPaymentTransaction(PaymentRequestDto request) {
        Validate.notNull(request, "Payment request cannot be null");

        log.info("Start payment transaction for {}", request);

        return createTransaction(request);
    }

    @Override
    public Transaction paymentTransactionComplete(String json) {
        return null;
    }

    @Override
    public List<PaymentMethodDto> getPaymentMethods(Transaction transaction, String returnUrl, URL baseUrl) {
        log.info("Get payment methods for transaction {}", transaction.getId());

        // Dummy payment methods
        List<PaymentMethodDto> paymentMethods = new ArrayList<>();

        paymentMethods.add(PaymentMethodDto.of("Swedbank", returnUrl, "swedbank.png", Country.EST, PaymentTypeDto.BANK));

        addLogoUrls(paymentMethods, baseUrl);

        return paymentMethods;
    }

    private Transaction createTransaction(PaymentRequestDto request){
        log.info("Create transaction for {}", request);

        Transaction transaction = new Transaction();
        transaction.setDateTime(ZonedDateTime.now().toLocalDateTime());
        transaction.setPaymentStatus(PaymentStatus.PENDING);
        transaction.setReference(request.getReference());
        transaction.setCustomerName(request.getCustomerName());
        transaction.setAdditionalData(request.getAdditionalData());
        transaction.setCountry(request.getCountry());
        transaction.setAmountDue(request.getAmount());

        return transactionService.create(transaction);
    }

    private void addLogoUrls(List<PaymentMethodDto> methods, URL baseUrl){
        for (PaymentMethodDto method : methods) {
            try {
                method.setLogo(UrlUtil.combine(baseUrl, String.format("/files/images/%s.png", method.getName())).toString());
            }
            catch (URISyntaxException | MalformedURLException ex) {
                log.error("Exception constructing logo url for: {}. Exception: {}", method.getName(), ex.getMessage(), ex);
            }
        }
    }
}
