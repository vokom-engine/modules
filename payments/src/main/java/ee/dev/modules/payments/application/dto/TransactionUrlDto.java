package ee.dev.modules.payments.application.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by bilal90.
 */
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper=false)
public class TransactionUrlDto {

    @JsonProperty("return_url")
    private UrlDto returnUrl;

    @JsonProperty("cancel_url")
    private UrlDto cancelUrl;

    @JsonProperty("notification_url")
    private UrlDto notificationUrl;
}
