package ee.dev.modules.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class StringUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(StringUtil.class);
    private static final String SEPARATOR = "/";
    private static final String ERROR_PARSING = "Error parsing {}. Exception: '{}'";

    private StringUtil() {
    }

    public static boolean isNullOrEmpty(String property){
        return property == null || property.trim().isEmpty();
    }

    public static String getSimpleDownloadLink(URL baseUrl, String driverId, String path){
        String baseUrlStr = baseUrl.toString();
        if(!baseUrlStr.endsWith(SEPARATOR)) baseUrlStr += SEPARATOR;

        return baseUrlStr + driverId + SEPARATOR + path;
    }

//    public static String getMessage(String code, String locale) {
//        return messageSource.getMessage(code, null, new Locale(locale));
//    }

    public static String replaceWhiteSpaces(String str, String replacement){
        return str.replaceAll("\\s+", replacement);
    }

    public static String trimRemoveWhiteSpaces(String str){
        return replaceWhiteSpaces(str, "").trim();
    }

    public static LocalTime parseLocalTime(String timeStr){
        if(timeStr == null) return null;

        DateTimeFormatter formatter;
        if(timeStr.length() > 5) formatter = DateTimeFormatter.ofPattern("H:mm:ss");
        else formatter = DateTimeFormatter.ofPattern("H:mm");

        try{
            return LocalTime.parse(timeStr, formatter);
        }catch (DateTimeParseException ex){
            LOGGER.error("Error parsing local time: {}", ex.getMessage());
            return null;
        }
    }

    public static String getTimeDurationFormatted(String dTimeStr, String aTimeStr, long hoursToAdd){
        try{

            String duration = getTimeDuration(dTimeStr, aTimeStr, hoursToAdd).toString();
            duration = duration.replace("PT","");

            StringBuilder fDuration = new StringBuilder();
            for (char c : duration.toCharArray()){
                fDuration.append(c);
                if(Character.isAlphabetic(c)){
                    fDuration.append(' ');
                }
            }

            return fDuration.toString().trim().toLowerCase();
        }catch (Exception ex){
            LOGGER.error("Error calculating duration: {}", ex.getMessage());
            return null;
        }
    }

    public static Duration getTimeDuration(String dTimeStr, String aTimeStr, long hoursToAdd){
        try{
            LocalTime dTime = parseLocalTime(dTimeStr);
            if(dTime == null){
                throw new IllegalArgumentException("Invalid Time !!!");
            }

            LocalTime aTime = parseLocalTime(aTimeStr);
            if(aTime == null){
                throw new IllegalArgumentException("Invalid Time !!!");
            }

            return Duration.between(dTime, aTime).abs().plusHours(hoursToAdd);
        }catch (Exception ex){
            LOGGER.error("Error calculating duration: {}", ex.getMessage());
            return Duration.ZERO;
        }
    }

    public static LocalDateTime parseDateTime(LocalDate date, String timeStr){
        try {
            LocalTime time = parseLocalTime(timeStr);
            //LocalDate date = LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE_TIME);
            //DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return LocalDateTime.of(date, time);
        }catch (Exception ex){
            LOGGER.error(ERROR_PARSING, timeStr, ex.getMessage());
            return null;
        }
    }

    public static LocalDate parseDate(String dateStr, String format){
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            return LocalDate.parse(dateStr, formatter);
        }catch (Exception ex){
            LOGGER.error(ERROR_PARSING, dateStr, ex.getMessage());
            return null;
        }
    }

    public static LocalDate parseDate(String dateStr){
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");
            return LocalDate.parse(dateStr, formatter);
        }catch (Exception ex){
            LOGGER.error(ERROR_PARSING, dateStr, ex.getMessage());
            return null;
        }
    }

    public static LocalTime parseTime(String timeStr){
        try {
            return LocalTime.parse(timeStr, DateTimeFormatter.ISO_DATE_TIME);
        }catch (Exception ex){
            LOGGER.error(ERROR_PARSING, timeStr, ex.getMessage());
            return null;
        }
    }

    public static String timeToString(LocalTime time){
        try {
            return time.format(DateTimeFormatter.ofPattern("H:m:s"));
        }catch (Exception ex){
            LOGGER.error(ERROR_PARSING, time, ex.getMessage());
            return null;
        }
    }

    public static LocalDate parseMonthYear(String dateStr){
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
            Date date = dateFormat.parse(dateStr);
            return new java.sql.Date(date.getTime()).toLocalDate();
        }catch (Exception ex){
            LOGGER.error(ERROR_PARSING, dateStr, ex.getMessage());
            return null;
        }
    }

    public static LocalDate parseYear(String dateStr){
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy");
            Date date = dateFormat.parse(dateStr);
            return new java.sql.Date(date.getTime()).toLocalDate();
        }catch (Exception ex){
            LOGGER.error(ERROR_PARSING, dateStr, ex.getMessage());
            return null;
        }
    }

    public static String createMacHashWithKey (String arg, String secretKey)
    {
        String sha512 = StringUtil.hash512String(arg + secretKey);
        if(sha512 != null) return sha512.toUpperCase();

        return null;
    }

    public static String hash512String(String arg){
        String generatedHash = null;

        byte[] argBytes = stringToBytes(arg);
        if(argBytes == null) return null;

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] bytes = md.digest(argBytes);
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            generatedHash = sb.toString();
        }
        catch (NoSuchAlgorithmException e){
            LOGGER.error(String.format("Error hashing %s, exception: %s ", arg, e.getMessage()));
        }

        return generatedHash;
    }

    public static byte[] stringToBytes(String arg){
        byte[] bytes = null;
        try {
            bytes = arg.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(String.format("Error getting %s bytes, exception: %s ", arg, e.getMessage()));
        }

        return bytes;
    }

    public static String stringToHex(String arg) {
        byte[] argBytes = stringToBytes(arg);
        if(argBytes == null) return null;

        return String.format("%040x", new BigInteger(1, argBytes));
    }

    public static String removeNonNumeric(final String input){
        return input.replaceAll("[^.,0-9]", "");
    }

    public static String getFirstXCharsInStr(final String str, int count){
        if(str == null || str.trim().isEmpty()) return null;

        if (str.length() > count) return str.substring(0, count).trim();
        else return str;
    }

    public static BigDecimal strToBigDecimal(final String value){
        if(value == null || value.trim().isEmpty()) return null;

        try {
            return new BigDecimal(removeNonNumeric(value).replace(',', '.'));
        }catch (Exception ex){
            LOGGER.error("Error parsing string value {}", value);
            return null;
        }
    }

    public static String toTitleCase(final String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for (char c : input.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextTitleCase = true;
            } else if (nextTitleCase) {
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();
    }

    public static boolean isStringNullOrEmpty(String property) {
        return property == null || property.trim().isEmpty();
    }

    public static int ordinalLastIndexOf(String str, String substr, int n) {
        int pos = str.lastIndexOf(substr);
        while (--n > 0 && pos != -1)
            pos = str.lastIndexOf(substr, pos - 1);
        return pos;
    }

    public static String replaceEstonianChars(String text){
        StringBuilder builder = new StringBuilder(text);

        Map<Character, Character> charMap = new HashMap<>();
        charMap.put('Š', 'S');
        charMap.put('Ž', 'Z');
        charMap.put('Õ', 'O');
        charMap.put('Ä', 'A');
        charMap.put('Ö', 'O');
        charMap.put('Ü', 'U');

        charMap.put('š', 's');
        charMap.put('ž', 'z');
        charMap.put('õ', 'o');
        charMap.put('ä', 'a');
        charMap.put('ö', 'o');
        charMap.put('ü', 'u');

        for (int i = 0; i < text.length(); i++) {
            Character ch = charMap.get(text.charAt(i));
            if(ch != null){
                builder.setCharAt(i, ch);
            }
        }

        return builder.toString();
    }
}
