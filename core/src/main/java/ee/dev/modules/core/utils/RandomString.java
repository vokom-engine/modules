package ee.dev.modules.core.utils;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

public class RandomString {
    /**
     *  public static final int DEFAULT_LENGTH = 8;
     *     private static final char[] SYMBOL;
     *     private static final int KEY_BITS;
     *     private final Random random;
     *     private final int length;
     *
     *     public RandomString() {
     *         this(8);
     *     }
     *
     *     public RandomString(int length) {
     *         if (length <= 0) {
     *             throw new IllegalArgumentException("A random string's length cannot be zero or negative");
     *         } else {
     *             this.length = length;
     *             this.random = new Random();
     *         }
     *     }
     *
     *     public static String make() {
     *         return make(8);
     *     }
     *
     *     public static String make(int length) {
     *         return (new RandomString(length)).nextString();
     *     }
     *
     *     public static String hashOf(int value) {
     *         char[] buffer = new char[32 / KEY_BITS + (32 % KEY_BITS == 0 ? 0 : 1)];
     *
     *         for(int index = 0; index < buffer.length; ++index) {
     *             buffer[index] = SYMBOL[value >>> index * KEY_BITS & -1 >>> 32 - KEY_BITS];
     *         }
     *
     *         return new String(buffer);
     *     }
     *
     *     public String nextString() {
     *         char[] buffer = new char[this.length];
     *
     *         for(int index = 0; index < this.length; ++index) {
     *             buffer[index] = SYMBOL[this.random.nextInt(SYMBOL.length)];
     *         }
     *
     *         return new String(buffer);
     *     }
     *
     *     static {
     *         StringBuilder symbol = new StringBuilder();
     *
     *         char character;
     *         for(character = '0'; character <= '9'; ++character) {
     *             symbol.append(character);
     *         }
     *
     *         for(character = 'a'; character <= 'z'; ++character) {
     *             symbol.append(character);
     *         }
     *
     *         for(character = 'A'; character <= 'Z'; ++character) {
     *             symbol.append(character);
     *         }
     *
     *         SYMBOL = symbol.toString().toCharArray();
     *         int bits = 32 - Integer.numberOfLeadingZeros(SYMBOL.length);
     *         KEY_BITS = bits - (Integer.bitCount(SYMBOL.length) == bits ? 0 : 1);
     *     }
     */
    private static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LOWER = UPPER.toLowerCase(Locale.ROOT);
    private static final String DIGITS = "0123456789";
    private static final String ALPHANUM = UPPER + LOWER + DIGITS;
    private static final int LENGTH = 21;

    private final Random random;
    private final char[] symbols;
    private final char[] buf;

    public RandomString(int length, Random random, String symbols) {
        if (length < 1) {
            throw new IllegalArgumentException();
        }

        if (symbols.length() < 2) {
            throw new IllegalArgumentException();
        }

        this.random = Objects.requireNonNull(random);
        this.symbols = symbols.toCharArray();
        this.buf = new char[length];
    }

    public static String random(int length) {
        String randomStr = String.valueOf(Double.doubleToLongBits(Math.random()));
        length = Math.min(length, randomStr.length());
        return randomStr.substring(0, length);
    }

    /**
     * Create an alphanumeric string generator.
     */
    public RandomString(int length, Random random) {
        this(length, random, ALPHANUM);
    }

    /**
     * Create an alphanumeric strings from a secure generator.
     */
    public RandomString(int length) {
        this(length, new SecureRandom());
    }

    /**
     * Create session identifiers.
     */
    public RandomString() {
        this(LENGTH);
    }

    public static RandomString randomUpperString(int length) {
        return new RandomString(length, new SecureRandom(), UPPER);
    }

    /**
     * Generate a random string.
     */
    public String nextString() {
        for (int idx = 0; idx < buf.length; ++idx){
            buf[idx] = symbols[random.nextInt(symbols.length)];
        }

        return new String(buf);
    }
}
