package ee.dev.modules.core.domain.model;

/**
 * Created by bilal90.
 */
public enum Currency {
    EUR("€"),
    USD("$"),
    NRN("₦");

    private final String symbol;

    Currency(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
