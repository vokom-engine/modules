package ee.dev.modules.core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class FileAttribute {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileAttribute.class);

    private BasicFileAttributes attributes = null;

    public FileAttribute(File file) throws IOException {
        Path filePath = file.toPath();
        attributes = Files.readAttributes(filePath, BasicFileAttributes.class);
    }

    public String getCreationTime(){
        return creationTimeToDate(attributes.creationTime());
    }

    public String getLastModifiedTime(){
        return creationTimeToDate(attributes.lastModifiedTime());
    }

    private String creationTimeToDate(FileTime time){
        return LocalDateTime.ofInstant(time.toInstant(), ZoneOffset.ofHours(2)).toString();
    }
}
