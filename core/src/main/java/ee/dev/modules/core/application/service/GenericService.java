package ee.dev.modules.core.application.service;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GenericService<T> {
    /**
     * Create entity
     * @param entity to create
     * @return created entity
     */
    T create(T entity);

    /**
     * Create entity
     * @param entity to update
     * @return updated entity
     */
    T update(T entity);

    /**
     * Find one entity with given ID
     * @param id of enity to get
     * @return found entity
     */
    T findOne(String id);

    /**
     * Get all entities
     * @return list of entities
     */
    List<T> findAll();

    /**
     * Pageable result list
     * @param count
     * @param offset
     * @return entities
     */
    List<T> findAll(int count, int offset);

    /**
     * Delete entity with given ID
     * @param id of entity to delete
     */
    void delete(String id);

    /**
     * Get entities count
     * @return number of entities
     */
    long count();

    /**
     * Save all entities
     * @param entities to save
     * @return list of saved entities
     */
    List<T> saveAll(List<T> entities);
}
