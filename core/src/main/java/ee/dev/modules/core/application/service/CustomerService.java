package ee.dev.modules.core.application.service;


import ee.dev.modules.core.domain.model.Customer;

/**
 *
 */
public interface CustomerService extends GenericService<Customer> {
}
