package ee.dev.modules.core.domain.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotEmpty;

/**
 * Created by bilal90.
 */
@Embeddable
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
public class Address extends BaseEntity {

    @NotEmpty
    @NonNull
    @Column(name = "street")
    private String street;

    @NotEmpty
    @NonNull
    @Column(name = "city")
    private String city;

    @NotEmpty
    @NonNull
    @Column(name = "state")
    private String state;

    @Column(name = "zip_code")
    private String zipCode;

    @NotEmpty
    @NonNull
    @Column(name = "country")
    private String country;
}
