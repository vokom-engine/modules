package ee.dev.modules.core.configuration;

import ee.dev.modules.core.utils.FileUtil;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {
    private final FolderConfiguration config;

    public ApplicationStartup(FolderConfiguration config) {
        this.config = config;
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        createDirectories();
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setCacheSeconds(-1);
        messageSource.setFallbackToSystemLocale(false);

        return messageSource;
    }

    private void createDirectories() {
        String imageDirPath = config.getFilesFolder() + config.getImageFolder();
        String ticketsDirPath = config.getFilesFolder() + config.getTicketsFolder();
        String dataDirPath = config.getFilesFolder() + config.getDataFolder();

        FileUtil.createDirectory(imageDirPath);
        FileUtil.createDirectory(ticketsDirPath);
        FileUtil.createDirectory(dataDirPath);
    }
}
