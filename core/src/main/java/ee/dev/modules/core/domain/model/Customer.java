package ee.dev.modules.core.domain.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 *
 */
@Entity
@Data
@Table
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
public class Customer extends AuditingEntity {

    @NotEmpty
    @NonNull
    @Column
    private String firstName;

    @Column
    private String lastName;

    @NonNull
    @Email
    @Column
    private String email;

    @Embedded
    private PhoneNumber phone;

    public String getFullName() {
        return firstName + " " + lastName;
    }
}
