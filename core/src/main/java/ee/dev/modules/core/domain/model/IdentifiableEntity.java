package ee.dev.modules.core.domain.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;

/**
 * Created by bilal90.
 */
@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public abstract class IdentifiableEntity extends BaseEntity {

    @Id
    @GeneratedValue(generator = "custom-sequence-generator", strategy = GenerationType.SEQUENCE)
    @GenericGenerator(name = "custom-sequence-generator", parameters = {
            @org.hibernate.annotations.Parameter(name = "prefix", value = "PPL_"),
            @org.hibernate.annotations.Parameter(name = "sequence_name", value = "user_sequence"),
            @org.hibernate.annotations.Parameter(name = "initial_value", value = "1000"),
            @Parameter(name = "increment_size", value = "1")
    }, strategy = "ee.dev.modules.core.domain.model.CustomSequenceGenerator")
    @Column(columnDefinition = "CHAR(32)")
    protected String id;
}
