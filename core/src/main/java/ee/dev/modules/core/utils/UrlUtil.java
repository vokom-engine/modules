package ee.dev.modules.core.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

@Slf4j
public class UrlUtil {
    //private static final Logger logger = LoggerFactory.getLogger(RequestUtil.class);

    /*public static URL getBaseUrl(HttpServletRequest request) throws MalformedURLException{
        try {
            return new URL(request.getScheme(), request.getServerName(), request.getServerPort(),
                    request.getContextPath().concat(request.getRequestURI()));
        }
        catch (MalformedURLException e) {
            log.error("Error constructing the base url '{}'", e.getMessage());

            throw  e;
        }
    }*/

    public static URL getBaseUrl(HttpServletRequest request) throws MalformedURLException{
        try {
            return new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
        }
        catch (MalformedURLException e) {
            log.error("Error constructing the base url '{}'", e.getMessage());

            throw  e;
        }
    }

    public static URL getUrl(HttpServletRequest request, String file) throws MalformedURLException {
        try {
            return new URL(request.getScheme(), request.getServerName(), request.getServerPort(), file);
        }
        catch (MalformedURLException e) {
            log.error("Error constructing the base url '{}'", e.getMessage());

            throw e;
        }
    }

    public static URL combine(URL url, String path) throws URISyntaxException, MalformedURLException {
        URL newUrl = new URL(url.getProtocol(), url.getHost(), url.getPort(), url.getFile() + path, null);
        return newUrl.toURI().normalize().toURL();
    }
}
