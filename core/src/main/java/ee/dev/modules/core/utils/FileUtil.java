package ee.dev.modules.core.utils;

import ee.dev.modules.core.application.dto.FileInfoDTO;
import ee.dev.modules.core.application.exception.StorageException;
import ee.dev.modules.core.application.exception.StorageFileNotFoundException;
import org.apache.tika.detect.Detector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AutoDetectParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

/**
 * Created by bilal90 on 10/27/2017.
 */
public class FileUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    private FileUtil() {
    }

    public static String getFileExtension(String name) {
        ValidationUtil.validatePropertyNotNullOrEmpty(name, "file name");
        int dotIndex = name.lastIndexOf('.');
        return (dotIndex == -1) ? "" : name.substring(dotIndex + 1);
    }

    public static void createDirectory(String dirPath){
        try {
            Path rootLocation = Paths.get(dirPath);
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            LOGGER.error("Failed trying to create the directory {}", e.getMessage());
        }
    }

    public static FileInfoDTO getFileInfo(MultipartFile file, String downloadLink){
        FileInfoDTO fileInfo = new FileInfoDTO();
        fileInfo.setDownloadUrl(downloadLink);
        fileInfo.setName(file.getOriginalFilename());
        fileInfo.setSize(file.getSize() / (float)(1024 * 1024));

        return fileInfo;
    }

    public static FileInfoDTO getFileInfo(MultipartFile file){
        return getFileInfo(file, null);
    }

    public static String getContentType(File file) {
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))) {
            AutoDetectParser parser = new AutoDetectParser();
            Detector detector = parser.getDetector();
            Metadata md = new Metadata();
            md.add(Metadata.RESOURCE_NAME_KEY, file.getName());
            MediaType mediaType = detector.detect(bis, md);

            return mediaType.toString();
        }catch (Exception ex){
            LOGGER.error("Error getting file content type: {}", ex);
            return null;
        }
    }

    public static String saveFile(MultipartFile file, String location) {
        Path path = Paths.get(location);
        String filename = file.getOriginalFilename();
        filename = StringUtil.replaceWhiteSpaces(filename, "-");
        try {
            if (file.isEmpty()) {
                throw new IllegalArgumentException("File Cannot be Empty!!!");
            }

            Files.copy(file.getInputStream(), path.resolve(filename),
                    StandardCopyOption.REPLACE_EXISTING);
            return path.resolve(filename).toAbsolutePath().toString();
        }
        catch (IOException e) {
            throw new StorageException("Failed to save file " + filename, e);
        }
    }

    /*public static FileInfoDTO savePriceFile(MultipartFile file, HttpServletRequest req, CoreConfiguration config){
        validateFile(file);

        validateFileFormat(file, new String[]{"xlsx", "xls"});

        String filename = file.getOriginalFilename();
        filename = StringUtils.replaceWhiteSpaces(filename, "-");

        URL url = RequestUtils.getUrl(req, "/files/");
        String downloadUrl = url + config.getPriceFileFolder() + filename;
        String dirPath = config.getTempFolderPath() + config.getPriceFileFolder();
        String savedPath = FileUtils.saveFile(file, dirPath);

        FileInfoDTO fileInfo = FileUtils.getFileInfo(file, downloadUrl);
        fileInfo.setFileAbsolutePath(savedPath);

        return fileInfo;
    }*/

    /*public static FileInfoDTO saveImageFile(MultipartFile file, HttpServletRequest req, CoreConfiguration config){
        if(file != null && !file.isEmpty()){
            validateFileFormat(file, new String[]{"png", "jpeg", "jpg"});

            String filename = file.getOriginalFilename();
            filename = StringUtils.replaceWhiteSpaces(filename, "-");

            URL url = RequestUtils.getUrl(req, "/files/");
            String downloadUrl = url + config.getImageFileFolder() + filename;
            String dirPath = config.getTempFolderPath() + config.getImageFileFolder();
            String savedPath = FileUtils.saveFile(file, dirPath);

            FileInfoDTO fileInfo = FileUtils.getFileInfo(file, downloadUrl);
            fileInfo.setFileAbsolutePath(savedPath);

            return fileInfo;
        }

        return new FileInfoDTO();
    }*/

    public static boolean isValidFormat(MultipartFile file, String[] extensions){
        String ext = FileUtil.getFileExtension(file.getOriginalFilename());
        for (String extension : extensions) {
            if(ext.equalsIgnoreCase(extension)){
                return true;
            }
        }

        return false;
    }

    public static Stream<Path> loadAll(String location) {
        try {
            Path rootLocation = Paths.get(location);
            return Files.walk(rootLocation, 1)
                    .filter(path -> !path.equals(rootLocation))
                    .map(rootLocation::relativize);
        }
        catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }
    }

//    public static Path load(String filename) {
//        Path rootLocation = Paths.get(filename);
//        return rootLocation.resolve(filename);
//    }

    public static Resource loadAsResource(String filename) {
        try {
            Path file = Paths.get(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);
            }
        }
        catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    public void deleteAll(String location) {
        Path path = Paths.get(location);
        FileSystemUtils.deleteRecursively(path.toFile());
    }

    private static void validateFile(MultipartFile file){
        if(file == null || file.isEmpty()){
            throw new IllegalArgumentException("File cannot be null or empty");
        }
    }

    private static void validateFileFormat(MultipartFile file, String[] formats){
        boolean isValid = FileUtil.isValidFormat(file, formats);
        if(!isValid){
            throw new IllegalArgumentException("Invalid File Format!!!");
        }
    }
}
