package ee.dev.modules.core.application.service;

import ee.dev.modules.core.domain.model.BaseEntity;
import ee.dev.modules.core.domain.repository.OffsetBasedPageRequest;
import ee.dev.modules.core.utils.ValidationUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Slf4j
@Service
public abstract class BaseGenericService<R extends JpaRepository<E, String>, E extends BaseEntity> implements GenericService<E> {
    protected final R repository;

    protected BaseGenericService(R repository) {
        this.repository = repository;
    }

    @Override
    public E create(E entity) {
        log.info("Create entity: '{}'", entity);

        ValidationUtil.validateEntity(entity);

        return repository.saveAndFlush(entity);
    }

    @Override
    public E update(E entity){
        log.info("Update entity: '{}'", entity);

        ValidationUtil.validateEntity(entity);

        return repository.saveAndFlush(entity);
    }

    @Override
    public E findOne(String id) {
        log.info("Fetch one entity with id: '{}'", id);

        ValidationUtil.validateIdentity(id);

        return repository.findById(id).orElse(null);
    }

    @Override
    public List<E> findAll(){
        log.info("Fetch all entities");

        List<E> results = repository.findAll();
        if(results.isEmpty()) {
            log.info("No entities found");
            return new ArrayList<>();
        }

        return results;
    }

    @Override
    public List<E> findAll(int count, int offset) {
        return repository.findAll(new OffsetBasedPageRequest(offset, count)).getContent();
    }

    @Override
    public void delete(String id){
        log.info("Delete entity with id: '{}'", id);

        ValidationUtil.validateIdentity(id);

        E record = findOne(id);
        if(record != null){
            throw new IllegalArgumentException("Invalid Id");
        }

        repository.deleteById(id);
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    public List<E> saveAll(List<E> records) {
        ValidationUtil.validateEntity(records);

        return repository.saveAll(records);
    }
}
