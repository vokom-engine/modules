package ee.dev.modules.core.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;

/**
 * Created by bilal90.
 */
@Embeddable
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = true)
public class PhoneNumber extends BaseEntity {

    @NotEmpty
    @Column(name = "number")
    private String number;

    @Enumerated(EnumType.STRING)
    private Country country;
}
