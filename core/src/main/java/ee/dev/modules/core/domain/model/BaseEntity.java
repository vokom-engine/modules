package ee.dev.modules.core.domain.model;

import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * Created by bilal90.
 */
@Data
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

}