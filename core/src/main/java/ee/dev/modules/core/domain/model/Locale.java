package ee.dev.modules.core.domain.model;

/**
 * Created by bilal90.
 */
public enum Locale {
    ET,
    EN,
    RU,
    NG
}
