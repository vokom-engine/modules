package ee.dev.modules.core.application.service.impl;

import ee.dev.modules.core.application.service.BaseGenericService;
import ee.dev.modules.core.application.service.CustomerService;
import ee.dev.modules.core.domain.model.Customer;
import ee.dev.modules.core.domain.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 */
@Slf4j
@Service
public class CustomerServiceImpl extends BaseGenericService<CustomerRepository, Customer> implements CustomerService {

    @Autowired
    public CustomerServiceImpl(CustomerRepository repository) {
        super(repository);
    }
}
