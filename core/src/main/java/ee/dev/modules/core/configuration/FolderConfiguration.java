package ee.dev.modules.core.configuration;

import lombok.Getter;
import javax.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by bilal90 on 9/11/2019.
 */
@Configuration
@PropertySource(value = "classpath:file.properties", encoding="UTF-8")
public class FolderConfiguration {
    @Getter
    @NotBlank
    @Value("${files-path}")
    private String filesFolder;

    @Getter
    @NotBlank
    @Value("${image-folder}")
    private String imageFolder;

    @Getter
    @NotBlank
    @Value("${tickets-folder}")
    private String ticketsFolder;

    @Getter
    @NotBlank
    @Value("${data-folder}")
    private String dataFolder;
}
