package ee.dev.modules.core.domain.model;

/**
 * Created by bilal90.
 */
public enum Country {
    EST("Estonia", "+372"),
    NGA("Nigeria", "+234");

    private final String fullName;
    private final String phoneCode;

    Country(String fullName, String phoneCode) {
        this.fullName = fullName;
        this.phoneCode = phoneCode;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public String getFullName() {
        return fullName;
    }
}
