package ee.dev.modules.core.domain.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDateTime;

/**
 *
 */
@Embeddable
@Data
@NoArgsConstructor(force=true)
@AllArgsConstructor(staticName="of")
@EqualsAndHashCode(callSuper = true)
public class DateTimePeriod extends BaseEntity {

    @NonNull
    @Column
    private LocalDateTime startDateTime;

    @NonNull
    @Column
    private LocalDateTime endDateTime;
}
