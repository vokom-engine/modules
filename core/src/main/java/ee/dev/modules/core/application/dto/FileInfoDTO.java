package ee.dev.modules.core.application.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

/**
 * Created by bilal90
 */
@Data
@AllArgsConstructor(staticName = "of", access = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor(force = true)
public class FileInfoDTO {

    private String name;

    private String downloadUrl;

    @JsonIgnore
    private String fileAbsolutePath;

    private float size;
}
