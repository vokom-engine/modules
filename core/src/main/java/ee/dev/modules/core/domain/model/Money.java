package ee.dev.modules.core.domain.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;

/**
 * Created by bilal90.
 */
@Embeddable
@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@EqualsAndHashCode(callSuper = false)
public class Money extends BaseEntity{

    @NonNull
    @Column(name = "amount")
    private BigDecimal amount;

    @NonNull
    @Enumerated(EnumType.STRING)
    private Currency currency;
}
