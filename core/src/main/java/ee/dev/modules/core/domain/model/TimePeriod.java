package ee.dev.modules.core.domain.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Created by bilal90.
 */
@Embeddable
@Data
@NoArgsConstructor(force=true)
@AllArgsConstructor(staticName="of")
@EqualsAndHashCode(callSuper = false)
public class TimePeriod extends BaseEntity {

    @NonNull
    @Column(name = "start_time")
    private String startTime;

    @NonNull
    @Column(name = "end_time")
    private String endTime;
}
