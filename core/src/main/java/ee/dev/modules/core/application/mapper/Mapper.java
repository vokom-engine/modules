package ee.dev.modules.core.application.mapper;

import org.modelmapper.ModelMapper;

public final class Mapper {
    private Mapper(){ }

    private static class Holder {
        private static final ModelMapper INSTANCE = new ModelMapper();
    }

    public static ModelMapper getModelMapper() {
        return Holder.INSTANCE;
    }
}
